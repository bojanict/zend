<?php


class API_SoapFunctions {
	protected $con = null;
	
	private function konekcija() {
		if ($this->con == null) {
			$this->con = new Application_Model_Service();
		}
	}
	
	public function lekovi() {
		$this->konekcija();
		$apoteka = $this->con->svilekovi();
		return $apoteka;
	}
	
	public function kategorije() {
		$this->konekcija();
		$apoteka = $this->con->svekategorije();
		return $apoteka;
	}
	
	public function ponazivu($post) {
		$this->konekcija();
		$apoteka = $this->con->ponazivu($post);
		return $apoteka;
	}
	
	public function dodajlek($post) {
		$this->konekcija();
		$apoteka = $this->con->dodajlek($post);
		return $apoteka;
	}
	
	public function dodajkat($post) {
		$this->konekcija();
		$apoteka = $this->con->dodajkat($post);
		return $apoteka;
	}
	
	public function pokat($post) {
		$this->konekcija();
		$apoteka = $this->con->pokat($post);
		return $apoteka;
	}
}