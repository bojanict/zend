<?php

class    LoginController extends Zend_Controller_Action {
	
	protected $logged = null;
	
	public function init() {
		$auth = Zend_Auth::getInstance();
		$this->logged = $auth->hasIdentity();
		if (!empty($this->logged)) {
			$this->view->loglink = $this->view->url(array(
				'controller' => 'Login',
				'action' => 'logout'
			), 'default', true);
			$this->view->logtext = 'Logout';
			$authSession = new    Zend_Auth_Storage_Session();
		} else {
			$this->view->loglink = $this->view->url(array(
				'controller' => 'Login',
				'action' => 'login'
			), 'default', true);
			$this->view->logtext = 'Login';
		}
	}
	
	public function indexAction() {
		$this->view->headTitle()->prepend("Login");
	}
	
	public function loginAction() {
		if (!$this->logged) {
			$loginForm = new    Zend_Form();
			$loginForm->setAction('/Login/login')->setMethod('post');
			$loginForm->setAttrib('id', 'loginForm');
			$loginForm->setAttrib('name', 'loginForm');
			$loginForm->setAttrib('style', 'text-align:center');
			//$loginForm->setDecorators(array('FormElements'));
			
			$username = new    Zend_Form_Element_Text('tbUsername');
			$username->setLabel('Username:');
			$username->setRequired(true);
			$username->addValidator('regex', false, array('/^[A-Za-z0-9]*$/'))->addErrorMessage('Invalid username!');
			$username->setAttrib('id', 'tbUsername');
			$username->setAttrib('style', 'width:300px;margin:0px auto;color:black;');
			$username->addFilter('StringToLower');
			
			$password = new    Zend_Form_Element_Password('tbPassword');
			$password->setLabel('Password:');
			$password->setRequired(true);
			$password->addValidator('regex', false, array('/^[A-Za-z0-9]*$/'));
			$password->setAttrib('id', 'tbPassword');
			$password->setAttrib('style', 'width:300px;margin:0px auto;color:black;');
			
			$submit = new    Zend_Form_Element_Submit('btnLogin');
			$submit->setLabel('Login');
			$submit->setAttrib('id', 'btnSubmit');
			
			$loginForm->addElement($username);
			$loginForm->addElement($password);
			$loginForm->addElement($submit);
			
			$loginForm->setDecorators(array(
				'ViewHelper',
				'Error',
				array(
					array('data' => 'HtmlTag'),
					array(
						'tag' => 'td',
						'class' => 'element'
					)
				),
				array(
					'Label',
					array('tag' => 'td')
				),
				array(
					array('row' => 'HtmlTag'),
					array('tag' => 'tr')
				)
			));
			
			$loginForm->setDecorators(array(
				'FormElements',
				array(
					'HtmlTag',
					array('tag' => 'table')
				),
				'Form',
				array(
					'Fieldset',
					array('legend' => 'Login')
				)
			));
			$this->view->form = $loginForm;
		}
		$request = $this->getRequest();
		
		if ($request->isPost() && $loginForm->isValid($request->getPost())) {
			$data = $loginForm->getValues();
			$username = DBC::prepareString($data['tbUsername']);
			$password = DBC::prepareString($data['tbPassword']);
			$auth = Zend_Auth::getInstance();
			$user = new    Application_Model_DbTable_User();
			$authAdapter = new    Zend_Auth_Adapter_DbTable($user->getAdapter(), 'users');
			
			$authAdapter->setIdentityColumn('username')->setCredentialColumn('password');
			$authAdapter->setIdentity($username)->setCredential($password);
			$result = $auth->authenticate($authAdapter);
			if ($result->isValid()) {
				$session = new    Zend_Auth_Storage_Session();
				$data = $authAdapter->getResultRowObject(null, 'password');
				
				$userMapper = new    Application_Model_UserMapper();
				$registeredUser = new    Application_Model_User();
				$userMapper->find($data->userid, $registeredUser);
				
				$session->write($registeredUser);
				
				if ($registeredUser->getRoleid() == "2") {
					$this->_redirect('/Admin');
				}
				$this->_redirect('/Products');
			} else {
				$view = $this->_helper->layout();
				$view->err = "Incorrect username or password!";
			}
		}
	}
	
	public function logoutAction() {
		$this->_helper->viewRenderer->setNoRender(true);
		if ($this->logged) {
			$session = new    Zend_Auth_Storage_Session();
			$session->clear();
			$this->_redirect('/Index');
		}
	}
	
	public function registerAction() {
		$this->view->headTitle()->prepend("Register");
		$registerForm = new    Application_Form_Register();
		$request = $this->getRequest();
		$msg = '';
		if ($request->isPost() && $registerForm->isValid($_POST)) {
			$data = $request->getParams();
			$user = new    Application_Model_User();
			$userMapper = new    Application_Model_UserMapper();
			
			$user->setName(DBC::prepareString($data['tbName']));
			$user->setLastname(DBC::prepareString($data['tbLastname']));
			$user->setEmail(DBC::prepareString($data['tbEmail']));
			$user->setUsername(DBC::prepareString($data['tbUsername']));
			$user->setRoleid('1');
			$user->setPassword(DBC::prepareString($data['tbPassword']));
			
			$exists = DBC::fetch("SELECT * FROM users WHERE username = '{$user->getUsername()}';");
			if (empty($exists)) {
				$userMapper->save($user);
				$this->_redirect('/Login/login');
			} else {
				$msg = "Please enter different username";
			}
		}
		$this->view->msg = $msg;
		$this->view->register = $registerForm;
	}
	
}
