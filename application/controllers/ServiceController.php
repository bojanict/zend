<?php

class ServiceController extends Zend_Controller_Action {
	
	public function init() {
		$auth = Zend_Auth::getInstance();
		$isLogged = $auth->hasIdentity();
		if (!$isLogged) {
			$this->_redirect("/Index");
		}
		$this->_helper->viewRenderer->setNoRender(true);
	}
	
	public function indexAction() {
		$this->view->headTitle()->prepend("Service");
		//TODO: Probaj ovde da ubacis WebServis. Pregled stanja svojih servisa i zakazivanje novog
	}
	
	public function soapAction() {
		$this->_helper->layout()->disableLayout();
		$this->getHelper('viewRenderer')->setNoRender(true);
		
		$soap = new Zend_Soap_Server(null, array(
			'uri' => 'http://zend/Service/soap'
		));
		$soap->setClass('API_SoapFunctions');
		$soap->handle();
	}
	
	public function wsdlAction() {
		$this->_helper->layout()->disableLayout();
		$this->getHelper('viewRenderer')->setNoRender(true);
		$wsdl = new Zend_Soap_AutoDiscover();
		
		$wsdl->setUri('http://zend/Service/soap');
		$wsdl->setClass('API_SoapFunctions');
		$wsdl->handle();
		
		/////TODO: Stavio je u public folder lek.php koji je nekako pozivao. meni taj kod trenutno stoji u index.phtml u Service folderu
	}
	
}
