<?php

class    ProductsController extends Zend_Controller_Action {
	
	public function init() {
		$this->view->headTitle()->prepend("Products");
		$this->view->headScript()->appendFile("/bootstrap/js/bootstrap.js");
		$this->view->headScript()->appendFile("/bootstrap/js/npm.js");
		$this->view->headLink()->appendStylesheet("/bootstrap/css/bootstrap-theme.min.css");
		$this->view->headLink()->appendStylesheet("/bootstrap/css/bootstrap-theme.css");
		$this->view->headLink()->appendStylesheet("/bootstrap/css/bootstrap.min.css");
		$this->view->headLink()->appendStylesheet("/bootstrap/css/bootstrap.css");
	}
	
	public function indexAction() {
		$request = $this->getRequest();
		$brand = empty($request->getParams()['brandid']) ? null : intval($request->getParams()['brandid']);
		$sql = "SELECT b.name AS brand, p.*, pi.product_image FROM brands b INNER JOIN products p "
			. "ON b.brandid = p.brandid LEFT JOIN product_images pi ON "
			. "p.productid = pi.productid WHERE 1 ";
		if (!empty($brand)) {
			$sql .= " AND b.brandid = '{$brand}' ;";
		}
		$products = DBC::fetchAll($sql);
		$data = array();
		foreach ($products as $product) {
			$sql = "SELECT c.*, CONCAT_WS(' ', u.lastname, u.name) as fullname FROM comments c INNER JOIN users u "
				. "ON c.userid = u.userid WHERE productid = {$product['productid']} ORDER BY created DESC;";
			$comments = DBC::fetchAll($sql);
			$numcomments = count($comments);
			$_product = array(
				"product" => $product,
				"numcomments" => empty($numcomments) ? 0 : $numcomments,
				"comments" => empty($comments) ? array() : $comments,
			);
//												echo "<pre>";var_dump($_product);echo "</pre>";die;
			array_push($data, $_product);
		}
		
		$sql = "SELECT * FROM brands;";
		$brands = DBC::fetchAll($sql);
		
		$insertCommentForm = new    Application_Form_Comment();
		$msg = '';
		if ($request->isPost() && $insertCommentForm->isValid($_POST)) {
			$insertCommentForm->reset();
			$res = $this->commentsAction($request->getParams());
			$msg = 'Comment added!';
			if (empty($res)) {
				$msg = "Comment not added!";
			}
		}
		$productList = new    Application_Model_ListProducts();
		$products = $productList->listProducts();
		if (!empty($brand)) {
			$products->where("brands.brandid = {$brand}");
		}
		$paginator = new    Zend_Paginator(new    Zend_Paginator_Adapter_DbSelect($products));
		$paginator->setItemCountPerPage(2);
		$paginator->setCurrentPageNumber($this->_getParam('page', 1));
		
		$this->view->paginator = $paginator;
		$this->view->msg = $msg;
		$this->view->products = $data;
		$this->view->brands = $brands;
		$this->view->insertCommentForm = $insertCommentForm;
	}
	
	public function commentsAction($request) {
		$auth = Zend_Auth::getInstance();
		$loggedUser = $auth->getIdentity();
		$productid = DBC::prepareString($request['hiddenId']);
		$comment = DBC::prepareString($request['tbComment']);
		$userid = $loggedUser->getUserid();
		
		if (!empty($userid)) {
			$sql = "INSERT INTO comments VALUES(NULL, {$productid}, {$userid}, '{$comment}', NOW(), NOW());";
			$res = DBC::exec($sql);
		}
		
		return $res;
	}
	
}
