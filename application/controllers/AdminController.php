<?php

class AdminController extends Zend_Controller_Action {
	
	public function init() {
		$auth = Zend_Auth::getInstance();
		$loggedUser = $auth->getIdentity();
		if (intval($loggedUser->getRoleid()) !== ROLE_ADMIN) {
			$this->_redirect("/Index");
		}
		
		$this->view->headScript()->appendFile("/bootstrap/js/bootstrap.js");
		$this->view->headScript()->appendFile("/bootstrap/js/npm.js");
		$this->view->headLink()->appendStylesheet("/bootstrap/css/bootstrap-theme.min.css");
		$this->view->headLink()->appendStylesheet("/bootstrap/css/bootstrap-theme.css");
		$this->view->headLink()->appendStylesheet("/bootstrap/css/bootstrap.min.css");
		$this->view->headLink()->appendStylesheet("/bootstrap/css/bootstrap.css");
		
		$_allUsers = new    Application_Model_UserMapper();
		$allUsers = $_allUsers->fetchAll();
		$this->view->allUsers = $allUsers;
	}
	
	public function indexAction() {
		$this->view->headTitle()->prepend("Admin");
		// USERS
		$userForm = new    Application_Form_User();
		$request = $this->getRequest();
		
		if ($request->isPost() && $userForm->isValid($_POST)) {
			$userForm->reset();
			$res = $this->adduserAction($request->getParams());
			if (empty($res)) {
				$this->view->msg = "Error inserting user";
			}
		}
		$this->view->addUserForm = $userForm;
		// END-USERS
		// LINKS
		$linksForm = new    Application_Form_Links();
		if ($request->isPost() && $linksForm->isValid($_POST)) {
			$linksForm->reset();
			$res = $this->addlinkAction($request->getParams());
			if (empty($res)) {
				$this->view->msg = "Error inserting menu link";
			}
		}
		$this->view->addLinkForm = $linksForm;
		$allLinks = new    Application_Model_Link();
		$table_data = array();
		foreach ($allLinks->fetchAll() as $row) {
			$admin_links = array(
				$this->view->outputLink($this->view->url(array(
					'controller' => 'Admin',
					'action' => 'links',
					'operation' => 'delete',
					'linkid' => $row['linkid']
				), 'default', true), 'Delete')
			);
			unset($row[0]);
			unset($row[1]);
			unset($row[2]);
			unset($row[3]);
			unset($row[4]);
			unset($row[5]);
			unset($row['linkid']);
			unset($row['created']);
			unset($row['modified']);
			
			$table_data[] = array_merge($row, $admin_links);
		}
		$this->view->tableLinks = $this->view->displayGenericTableHelper($table_data, array(
			'Link name',
			'Link controller',
			'Link action',
			''
		), 1);
		// END-LINKS
		// PRODUCTS
		$productForm = new    Application_Form_Product();
		$productMapper = new    Application_Model_ProductMapper();
		if ($request->isPost() && $productForm->isValid($_POST)) {
			$params = $request->getParams();
			$name = DBC::prepareString($params['tbName']);
			$brandid = DBC::prepareString($params['ddlBrandId']);
			$price = floatval($params['tbPrice']);
			$description = DBC::prepareString($params['taDescription']);
			$file = $productForm->getValues();
			$productid = empty($params['productid']) ? null : intval($params['productid']);
			
			$productModel = new    Application_Model_Product();
			$productModel->setName($name);
			$productModel->setBrandid($brandid);
			$productModel->setPrice($price);
			$productModel->setDescription($description);
			
			if (empty($productid)) {
				$_productid = $productMapper->save($productModel);
				$upload = new    Zend_File_Transfer_Adapter_Http();
				$path = APPLICATION_PATH . "/../public/images/products/product_{$_productid}";
				if (!file_exists($path)) {
					mkdir($path, 0777, true);
				}
				$upload->setDestination("{$path}/");
				$files = $upload->getFileInfo();
				foreach ($files as $file => $fileInfo) {
					if ($upload->isUploaded($file)) {
						if ($upload->isValid($file)) {
							if ($upload->receive($file)) {
								$info = $upload->getFileInfo($file);
								$_name = $info[$file]['name'];
								DBC::exec("INSERT INTO product_images VALUES({$_productid}, null, '/images/products/product_{$_productid}/{$_name}', NOW(), NOW());");
							}
						}
					}
				}
				$upload->receive();
			}
		}
		$table_data = array();
		foreach ($productMapper->fetchAll() as $row) {
			$admin_links = array(
				$this->view->outputLink($this->view->url(array(
					'controller' => 'Admin',
					'action' => 'products',
					'operation' => 'delete',
					'productid' => $row['productid']
				), 'default', true), 'Delete')
			);
			unset($row['productid']);
			$res = DBC::fetch("SELECT name AS brand FROM brands WHERE brandid = {$row['brandid']}");
			
			$row['brandid'] = $res['brand'];
			$table_data[] = array_merge($row, $admin_links);
		}
		usort($table_data, function($a, $b) {
			return strcmp($a['brandid'], $b['brandid']);
		});
		$this->view->tableProducts = $this->view->displayGenericTableHelper($table_data, array(
			'Brand',
			'Product headline',
			'Product price',
			'Product description',
			''
		), 1);
		$this->view->formProducts = $productForm;
		// END-PRODUCTS
		// BRANDS
		$brandsForm = new    Application_Form_Brands();
		if ($request->isPost() && $brandsForm->isValid($_POST)) {
			$brandsForm->reset();
			$res = $this->addbrandAction($request->getParams());
			if (empty($res)) {
				$this->view->msg = "Error inserting brand";
			}
		}
		$this->view->formBrands = $brandsForm;
		$allBrands = new    Application_Model_Brand();
		$table_data = array();
		foreach ($allBrands->fetchAll() as $row) {
			$admin_links = array(
				$this->view->outputLink($this->view->url(array(
					'controller' => 'Admin',
					'action' => 'brands',
					'operation' => 'delete',
					'brandid' => $row['brandid']
				), 'default', true), 'Delete')
			);
			unset($row[0]);
			unset($row[1]);
			unset($row[2]);
			unset($row[3]);
			unset($row['brandid']);
			unset($row['created']);
			unset($row['modified']);
			
			$table_data[] = array_merge($row, $admin_links);
		}
		$this->view->tableBrands = $this->view->displayGenericTableHelper($table_data, array(
			'Brand name',
			''
		), 1);
		// END-BRANDS
		// SERVICES
		// END-SERVICES
	}
	
	public function deleteuserAction() {
		if ($this->getRequest()->isXmlHttpRequest()) {
			$this->_helper->layout()->disableLayout();
			if ($this->getRequest()->isPost()) {
				$userid = $_POST['userid'];
				$userMapper = new    Application_Model_UserMapper();
				$userMapper->delete($userid);
				$this->_redirect('/Admin');
			}
		}
	}
	
	public function adduserAction($user) {
		$sql = "SELECT * FROM users WHERE username = '{$user['tbUsername']}';";
		$res = DBC::fetch($sql);
		if (empty($res)) {
			DBC::exec("INSERT INTO users VALUES(NULL, '{$user['tbUsername']}', "
				. "'{$user['tbPassword']}', {$user['roleid']}, '{$user['tbName']}', "
				. "'{$user['tbLastname']}', '{$user['tbEmail']}', NOW(), NOW());");
			$this->_redirect('/Admin');
		}
	}
	
	public function addlinkAction($request) {
		$name = DBC::prepareString($request['tbName']);
		$url1 = DBC::prepareString($request['tbUrl1']);
		$url2 = empty($request['tbUrl2']) ? 'index' : DBC::prepareString($request['tbUrl2']);
		
		DBC::exec("INSERT INTO links VALUES(NULL, '{$name}', '{$url1}', '{$url2}', NOW(), NOW())");
		$this->_redirect('/Admin');
	}
	
	public function productsAction() {
		$request = $this->getRequest();
		$params = $request->getParams();
		$productid = intval($params['productid']);
		$operation = DBC::prepareString($params['operation']);
		
		$productMapper = new    Application_Model_ProductMapper();
		if (!empty($productid) && $operation === 'delete') {
			$productMapper->delete($productid);
		}
		
		$this->_redirect('/Admin');
	}
	
	public function linksAction() {
		$request = $this->getRequest();
		$params = $request->getParams();
		$linkid = intval($params['linkid']);
		$operation = DBC::prepareString($params['operation']);
		
		$link = new    Application_Model_Link();
		if (!empty($linkid) && $operation === 'delete') {
			$link->delete($linkid);
		}
		
		$this->_redirect('/Admin');
	}
	
	public function brandsAction() {
		$request = $this->getRequest();
		$params = $request->getParams();
		$brandid = intval($params['brandid']);
		$operation = DBC::prepareString($params['operation']);
		
		$brand = new    Application_Model_Brand();
		if (!empty($brandid) && $operation === 'delete') {
			$brand->delete($brandid);
		}
		
		$this->_redirect('/Admin');
	}
	
	public function addbrandAction($request) {
		$name = DBC::prepareString($request['tbName']);
		
		DBC::exec("INSERT INTO brands VALUES(NULL, '{$name}', NOW(), NOW())");
		$this->_redirect('/Admin');
	}
	
	
}


