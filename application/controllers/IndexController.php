<?php

class    IndexController extends Zend_Controller_Action {
	
	public function init() {
//								if	(!file_exists("images/brands/Seiko"))	{
//												var_dump("ne postoji");
//								} else {var_dump("postoji");} die;
	}
	
	public function indexAction() {
		$this->view->headTitle()->prepend("Home");
		$contactForm = new    Application_Form_Contact();
		$request = $this->getRequest();
		$msg = '';
		if ($request->isPost() && $contactForm->isValid($_POST)) {
			$contactForm->reset();
			$res = $this->contactAction($request->getParams());
			$msg = 'Message sent!';
			if (empty($res)) {
				$msg = "Message not sent!";
			}
		}
		$this->view->msg = $msg;
		$this->view->contact = $contactForm;
	}
	
	public function contactAction($request) {
		$mail = new    Zend_Mail();
		$mail->setBodyText($request['tbMessage']);
		$mail->setFrom($request['tbEmail']);
		$mail->addTo('bojan.radakovic.1066.15@ict.edu.rs');
		$mail->setSubject('Message from: ' . $request['tbName']);
		$res = $mail->send();
		return $res;
	}
	
}
