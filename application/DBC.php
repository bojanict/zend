<?php

class    DBC {
	
	private static $instance = null;
	
	private function __construct() {
		
	}
	
	public static function get($new = false) {
		if (empty(self::$instance) || $new) {
			$db_server = 'localhost';
			$db_port = '3306';
			$db_login = 'root';
			$db_pass = '';
			$db_name = 'baza';
			try {
				self::$instance = new    PDO("mysql:host=$db_server;port=$db_port;dbname=$db_name", "$db_login", "$db_pass", array(
					PDO::ATTR_PERSISTENT => false,
					PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
				));
				self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (Exception    $e) {
				self::$instance = null;
			}
		}
		return self::$instance;
	}
	
	public static function setAttribute($attribute, $value) {
		if (self::get()) {
			try {
				return self::$instance->setAttribute($attribute, $value);
			} catch (Exception    $e) {
				
			}
		} else {
			
		}
		return false;
	}
	
	public static function exec($sql) {
		if (self::get()) {
			try {
				return self::$instance->exec($sql);
			} catch (Exception    $e) {
				
			}
		} else {
			
		}
		return null;
	}
	
	public static function query($sql) {
		if (self::get()) {
			try {
				return self::$instance->query($sql);
			} catch (PDOException    $exception) {
				
			} catch (Exception    $e) {
				
			}
		} else {
			
		}
		return null;
	}
	
	public static function fetch($sql) {
		$result = self::query($sql);
		return empty($result) ? null : $result->fetch();
	}
	
	public static function fetchAll($sql, $safe = TRUE) {
		$result = self::query($sql);
		return empty($result) ? ($safe ? array() : null) : $result->fetchAll();
	}
	
	public static function lastInsertId() {
		return self::$instance->lastInsertId();
	}
	
	public static function prepareString($text, $encode = ENCODE_ALL_DATA, $from = ENCODING_FROM, $to = ENCODING_TO) {
		$text = addslashes(stripslashes(stripslashes(stripslashes(trim(html_entity_decode($text))))));
		if ($encode) {
			$enc = mb_detect_encoding($text, "$from", false);
			if (($enc == 'ASCII') and ($to == "UTF-8")) {
				$text = utf8_encode($text);
			} else {
				$text = mb_convert_encoding($text, "$to", mb_detect_encoding($text, "$from", false));
			}
		}
		return $text;
	}
	
}
