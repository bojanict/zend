<?php

if (isset($this->previous)) {
	echo "<a style='text-decoration:none;' href='" . $this->url(array('page' => $this->previous)) . "'>&laquo;</a> ";
}
foreach ($this->pagesInRange as $page) {
	if ($this->current != $page) {
		echo " <a style='text-decoration:none;' href='" . $this->url(array('page' => $page)) . "'>" . $page . "</a> ";
	} else {
		echo $page;
	}
}
if (isset($this->next)) {
	echo "<a style='text-decoration:none;' href='" . $this->url(array('page' => $this->next)) . "'>&raquo;</a>";
}

echo " out of " . count($this->pagesInRange);