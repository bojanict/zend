<?php

class    Site_View_Helper_DisplayGenericTableHelper extends Zend_View_Helper_Abstract {
	
	public $view;
	
	public function setView(Zend_View_Interface $view) {
		$this->view = $view;
	}
	
	public function displayGenericTableHelper($rows, $columns, $border = 0) {
		$table = "";
		if (count($rows) > 0) {
			$table .= '<table border="' . $border . '"><tr>';
			foreach ($columns as $column) {
				$table .= '<th>' . $column . '</th>';
			}
			foreach ($rows as $row) {
				$table .= '</tr><tr>';
				foreach ($row as $content) {
					$table .= '<td>' . $content . '</td>';
				}
			}
			$table .= '</tr></table>';
		}
		return $table;
	}
	
}
