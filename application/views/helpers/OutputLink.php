<?php

class    Site_View_Helper_OutputLink extends Zend_View_Helper_Abstract {
	
	public function outputLink($anchor, $description) {
		return '<a href="' . $anchor . '">' . $description . '</a>';
	}
	
}
