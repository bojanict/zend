<?php

require_once 'DBC.php';

class    Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
	
	protected function _initDoctype() {
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$view->doctype('XHTML1_STRICT');
	}
	
	protected function _initGlobals() {
		define('ROLE_ADMIN', 2);
		define('ROLE_USER', 1);
		define("ENCODING_FROM", "ASCII, UTF-8, ISO-8859-1, ISO-8859-15");
		define("ENCODING_TO", "UTF-8");
		define("ENCODE_ALL_DATA", true);
	}
	
	protected function _initPlaceholders() {
		$this->bootstrap('view');
		$view = $this->getResource('view');
		
		$view->headLink(array(
			'rel' => 'icon',
			'href' => "/images/newbanner.jpg",
		), 'PREPEND')->appendStylesheet('/assets/css/main.css');
		$view->headLink()->appendStylesheet("/assets/css/products.scss");
		$view->headScript()->appendFile('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js');
		$view->headScript()->appendFile("/assets/js/jquery.min.js");
		$view->headScript()->appendFile("/assets/js/skel.min.js");
		$view->headScript()->appendFile("/assets/js/util.js");
		$view->headScript()->appendFile("/assets/js/main.js");
		
		$view->headTitle('Watches')->setSeparator(" | ");
	}
	
	protected function _initMenu() {
		$this->bootstrap('view');
		$view = $this->getResource('view');
		$sql = "SELECT name, controller, action FROM links;";
		$res = DBC::fetchAll($sql);
		$links = array();
		foreach ($res as $r) {
			array_push($links, $r);
		}
		
		$view->links = $links;
	}
	
	protected function _baseUrl() {
		return $this->_baseUrl();
	}
	
	protected function _initDatabase() {
		$db = new Zend_Db_Adapter_Pdo_Mysql(array(
			'host' => 'localhost',
			'username' => 'user',
			'password' => '',
			'dbname' => 'baza'
		));
		Zend_Registry::set('baza', $db);
	}
	
}
