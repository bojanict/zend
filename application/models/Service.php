<?php

class Application_Model_Service {
	
	private $db;
	
	function __construct() {
		try {
			$this->db = Zend_Db_Table::getDefaultAdapter();
		} catch (Exception $ex) {
			print $ex->getMessage();
		}
	}
	
	public function svilekovi() {
		$statment = $this->db->select()->from('lek');
		return $this->db->fetchAll($statment);
	}
	
	public function svekategorije() {
		$statment = $this->db->select()->from('kategorije');
		return $this->db->fetchAll($statment);
	}
	
	public function ponazivu($post) {
		$statment = $this->db->select()->from('lek')->where('naziv LIKE "%' . $post . '%"');
		return $this->db->fetchAll($statment);
	}
	
	public function pokat($post) {
		$statment = $this->db->select()->from('lek')->where('id_kategorije=' . $post);
		return $this->db->fetchAll($statment);
	}
	
	public function dodajlek($post) {
		return $this->db->insert('lek', $post);
	}
	
	public function dodajkat($post) {
		$data = array(
			"naziv" => $post,
		);
		return $this->db->insert('kategorije', $data);
	}
}

