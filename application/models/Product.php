<?php

class    Application_Model_Product {
	
	protected $productid;
	protected $brandid;
	protected $name;
	protected $price;
	protected $description;
	protected $created;
	protected $modified;
	
	public function _set($name, $value) {
		
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw    new    Exception('Property not defined.');
		}
		$this->$method($value);
	}
	
	public function _get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw    new    Exception('Property not defined');
		}
	}
	
	public function setOptions(array    $options) {
		$methods = get_class_methods($this);
		
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	function getProductid() {
		return $this->productid;
	}
	
	function getBrandid() {
		return $this->brandid;
	}
	
	function getName() {
		return $this->name;
	}
	
	function getPrice() {
		return $this->price;
	}
	
	function getDescription() {
		return $this->description;
	}
	
	function getCreated() {
		return $this->created;
	}
	
	function getModified() {
		return $this->modified;
	}
	
	function setProductid($productid) {
		$this->productid = $productid;
	}
	
	function setBrandid($brandid) {
		$this->brandid = $brandid;
	}
	
	function setName($name) {
		$this->name = $name;
	}
	
	function setPrice($price) {
		$this->price = $price;
	}
	
	function setDescription($description) {
		$this->description = $description;
	}
	
	function setCreated($created) {
		$this->created = $created;
	}
	
	function setModified($modified) {
		$this->modified = $modified;
	}
	
}
