<?php

class    Application_Model_Brand {
	
	protected $brandid;
	protected $name;
	protected $created;
	protected $modified;
	
	public function fetch($brandid) {
		$linkid = intval($linkid);
		$res = DBC::fetch("SELECT * FROM brands WHERE brandid = {$brandid};");
		return $res;
	}
	
	public function fetchAll() {
		$res = DBC::fetchAll("SELECT * FROM brands;");
		return $res;
	}
	
	public function update(Application_Model_Brand $brand) {
		DBC::exec("UPDATE brands SET "
		. empty($brand->getName()) ? "" : "name = '{$brand->getName()}' "
			. "WHERE brandid = {$link->getLinkid()};");
	}
	
	public function delete($brandid) {
		$_brandid = intval($brandid);
		DBC::exec("DELETE FROM brands WHERE brandid = {$_brandid};");
	}
	
	public function insert(Application_Model_Brand $brand) {
		DBC::exec("INSERT INTO brands VALUES("
			. "NULL, "
			. "'{$brand->getName()}', "
			. "NOW(), "
			. "NOW());");
	}
	
	function getBrandid() {
		return $this->brandid;
	}
	
	function getName() {
		return $this->name;
	}
	
	function getCreated() {
		return $this->created;
	}
	
	function getModified() {
		return $this->modified;
	}
	
	function setBrandid($brandid) {
		$this->brandid = $brandid;
	}
	
	function setName($name) {
		$this->name = $name;
	}
	
	function setCreated($created) {
		$this->created = $created;
	}
	
	function setModified($modified) {
		$this->modified = $modified;
	}
	
	
}
