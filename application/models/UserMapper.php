<?php

class    Application_Model_UserMapper {
	
	protected $_dbTable;
	
	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new    $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw    new    Exception("Invalid table data gateway provided");
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	
	public function getDbTable() {
		if (null == $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_User');
		}
		
		return $this->_dbTable;
	}
	
	public function save(Application_Model_User $user) {
		
		$data = array(
			'userid' => $user->getUserid(),
			'username' => $user->getUsername(),
			'password' => $user->getPassword(),
			'roleid' => $user->getRoleid(),
			'name' => $user->getName(),
			'lastname' => $user->getLastname(),
			'email' => $user->getEmail(),
			'created' => 'NOW()',
			'modified' => 'NOW()',
		);
		if (null === ($userid = $user->getUserid())) {
			unset($data['userid']);
			$this->getDbTable()->insert($data);
		} else {
			$this->getDbTable()->update($data, array('userid=?' => $userid));
		}
	}
	
	public function find($userid, Application_Model_User $user) {
		$result = $this->getDbTable()->find($userid);
		if (count($result) == 0) {
			return;
		}
		$row = $result->current();
		$user->setUserid($row->userid);
		$user->setUsername($row->username);
		$user->setPassword($row->password);
		$user->setEmail($row->email);
		$user->setName($row->name);
		$user->setLastname($row->lastname);
		$user->setRoleid($row->roleid);
	}
	
	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		
		$users = array();
		
		foreach ($resultSet as $row) {
			$user = new    Application_Model_User();
			$user->setUserid($row->userid);
			$user->setUsername($row->username);
			$user->setPassword($row->password);
			$user->setEmail($row->email);
			$user->setName($row->name);
			$user->setLastname($row->lastname);
			$user->setRoleid($row->roleid);
			$users[] = $user;
		}
		
		return $users;
	}
	
	public function fetch() {
		$row = $this->getDbTable()->fetchRow();
		
		$user = new    Application_Model_User();
		$user->setUserid($row->userid);
		$user->setUsername($row->username);
		$user->setPassword($row->password);
		$user->setEmail($row->email);
		$user->setName($row->name);
		$user->setLastname($row->lastname);
		$user->setRoleid($row->roleid);
		
		return $user;
	}
	
	public function delete($userid) {
		$where = $this->getDbTable()->getAdapter()->quoteInto('userid=?', $userid);
		$this->getDbTable()->delete($where);
	}
	
}
