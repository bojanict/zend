<?php

class    Application_Model_DbTable_User extends Zend_Db_Table_Abstract {
	
	protected $_name = 'users';
	protected $_username = 'username';
	protected $_userid = 'userid';
	protected $_referenceMap = array(
		'roleid' => array(
			'columns' => array('roleid'),
			'refTableClass' => 'Application_Model_DbTable_Role',
			'refColumns' => array('roleid')
		),
	
	);
}
