<?php

class    Application_Model_ListProducts {
	
	public function listProducts() {
		$db = Zend_Db_Table::getDefaultAdapter();
		$selectProducts = new    Zend_Db_Select($db);
		$selectProducts->from('products')
			->joinLeft('brands', 'products.brandid = brands.brandid', 'name as brand')
			->joinLeft('product_images', 'products.productid = product_images.productid', 'product_image as image');
		
		return $selectProducts;
	}
}
