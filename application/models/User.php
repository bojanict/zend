<?php

class    Application_Model_User {
	
	protected $_userid;
	protected $_username;
	protected $_password;
	protected $_roleid;
	protected $_name;
	protected $_lastname;
	protected $_email;
	protected $_created;
	protected $_modified;
	
	public function _set($name, $value) {
		
		$method = 'set' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw    new    Exception('Property not defined.');
		}
		$this->$method($value);
	}
	
	public function _get($name) {
		$method = 'get' . $name;
		if (('mapper' == $name) || !method_exists($this, $method)) {
			throw    new    Exception('Property not defined');
		}
	}
	
	public function setOptions(array    $options) {
		$methods = get_class_methods($this);
		
		foreach ($options as $key => $value) {
			$method = 'set' . ucfirst($key);
			if (in_array($method, $methods)) {
				$this->$method($value);
			}
		}
		return $this;
	}
	
	function getUserid() {
		return $this->_userid;
	}
	
	function getUsername() {
		return $this->_username;
	}
	
	function getPassword() {
		return $this->_password;
	}
	
	function getRoleid() {
		return $this->_roleid;
	}
	
	function getName() {
		return $this->_name;
	}
	
	function getLastname() {
		return $this->_lastname;
	}
	
	function getEmail() {
		return $this->_email;
	}
	
	function getCreated() {
		return $this->_created;
	}
	
	function getModified() {
		return $this->_modified;
	}
	
	function setUserid($_userid) {
		$this->_userid = $_userid;
	}
	
	function setUsername($_username) {
		$this->_username = $_username;
	}
	
	function setPassword($_password) {
		$this->_password = $_password;
	}
	
	function setRoleid($_roleid) {
		$this->_roleid = $_roleid;
	}
	
	function setName($_name) {
		$this->_name = $_name;
	}
	
	function setLastname($_lastname) {
		$this->_lastname = $_lastname;
	}
	
	function setEmail($_email) {
		$this->_email = $_email;
	}
	
	function setCreated($_created) {
		$this->_created = $_created;
	}
	
	function setModified($_modified) {
		$this->_modified = $_modified;
	}
	
}
