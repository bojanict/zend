<?php

class    Application_Model_ProductMapper {
	
	protected $_dbTable;
	
	public function setDbTable($dbTable) {
		if (is_string($dbTable)) {
			$dbTable = new    $dbTable();
		}
		if (!$dbTable instanceof Zend_Db_Table_Abstract) {
			throw    new    Exception("Invalid table data gateway provided");
		}
		$this->_dbTable = $dbTable;
		return $this;
	}
	
	public function getDbTable() {
		if (null == $this->_dbTable) {
			$this->setDbTable('Application_Model_DbTable_Product');
		}
		
		return $this->_dbTable;
	}
	
	public function save(Application_Model_Product $product) {
		$id = null;
		$data = array(
			'productid' => $product->getProductid(),
			'brandid' => $product->getBrandid(),
			'name' => $product->getName(),
			'price' => $product->getPrice(),
			'description' => $product->getDescription(),
			'created' => 'NOW()',
			'modified' => 'NOW()',
		);
		if (empty($product->getProductid())) {
			unset($data['productid']);
			$id = $this->getDbTable()->insert($data);
		} else {
			$this->getDbTable()->update($data, array('productid=?' => $product->getProductid()));
		}
		return $id;
	}
	
	public function find($productid, Application_Model_Product $product) {
		$result = $this->getDbTable()->find($productid);
		if (count($result) == 0) {
			return;
		}
		$row = $result->current();
		$product->setProductid($row->productid);
		$product->setBrandid($row->brandid);
		$product->setName($row->name);
		$product->setPrice($row->price);
		$product->setDescription($row->description);
	}
	
	public function fetchAll() {
		$resultSet = $this->getDbTable()->fetchAll();
		
		$products = array();
		
		foreach ($resultSet as $row) {
			$product['productid'] = $row->productid;
			$product['brandid'] = $row->brandid;
			$product['name'] = $row->name;
			$product['price'] = $row->price;
			$product['description'] = $row->description;
			$products[] = $product;
		}
		
		return $products;
	}
	
	public function fetch() {
		$row = $this->getDbTable()->fetchRow();
		
		$product = new    Application_Model_Product();
		$product->setProductid($row->productid);
		$product->setBrandid($row->brandid);
		$product->setName($row->name);
		$product->setPrice($row->price);
		$product->setDescription($row->description);
		
		return $product;
	}
	
	public function delete($productid) {
		$where = $this->getDbTable()->getAdapter()->quoteInto('productid=?', $productid);
		$this->getDbTable()->delete($where);
	}
	
}
