<?php

class    Application_Model_Link {
	
	protected $linkid;
	protected $name;
	protected $controller;
	protected $action;
	protected $created;
	protected $modified;
	
	public function fetch($linkid) {
		$linkid = intval($linkid);
		$res = DBC::fetch("SELECT * FROM links WHERE linkid = {$linkid};");
		return $res;
	}
	
	public function fetchAll() {
		$res = DBC::fetchAll("SELECT * FROM links;");
		return $res;
	}
	
	public function update(Application_Model_Link $link) {
		DBC::exec("UPDATE links SET "
		. empty($link->getName()) ? "" : "name = '{$link->getName()}', "
		. empty($link->getController()) ? "" : "controller = '{$link->getController()}', "
		. empty($link->getAction()) ? "" : "action = '{$link->getAction()}' "
			. "WHERE linkid = {$link->getLinkid()};");
	}
	
	public function delete($linkid) {
		$linkid = intval($linkid);
		DBC::exec("DELETE FROM links WHERE linkid = {$linkid};");
	}
	
	public function insert(Application_Model_Link $link) {
		DBC::exec("INSERT INTO links VALUES("
			. "NULL, "
			. "'{$link->getName()}', "
			. "'{$link->getController()}', "
			. "'{$link->getAction()}', "
			. "NOW(), "
			. "NOW());");
	}
	
	function getLinkid() {
		return $this->linkid;
	}
	
	function getName() {
		return $this->name;
	}
	
	function getController() {
		return $this->controller;
	}
	
	function getAction() {
		return $this->action;
	}
	
	function getCreated() {
		return $this->created;
	}
	
	function getModified() {
		return $this->modified;
	}
	
	function setLinkid($linkid) {
		$this->linkid = $linkid;
	}
	
	function setName($name) {
		$this->name = $name;
	}
	
	function setController($controller) {
		$this->controller = $controller;
	}
	
	function setAction($action) {
		$this->action = $action;
	}
	
	function setCreated($created) {
		$this->created = $created;
	}
	
	function setModified($modified) {
		$this->modified = $modified;
	}
	
	
}
