<?php

class    Application_Form_User extends Zend_Form {
	
	public function init() {
		$this->setAction('/Admin/index')->setMethod('post');
		
		$name = new    Zend_Form_Element_Text('tbName');
		$name->setAttrib('placeholder', 'Name');
		$name->setAttrib('id', 'tbName');
		
		$lastname = new    Zend_Form_Element_Text('tbLastname');
		$lastname->setAttrib('placeholder', 'Lastname');
		$lastname->setAttrib('id', 'tbLastname');
		
		$email = new    Zend_Form_Element_Text('tbEmail');
		$email->setAttrib('placeholder', 'Email');
		$email->setAttrib('id', 'tbEmail');
		
		$username = new    Zend_Form_Element_Text('tbUsername');
		$username->setAttrib('placeholder', 'Username');
		$username->setAttrib('id', 'tbUsername');
		$username->setRequired(true);
		
		$password = new    Zend_Form_Element_Password('tbPassword');
		$password->setAttrib('placeholder', 'Password');
		$password->setAttrib('id', 'tbPassword');
		$password->setRequired(true);
		
		$roleid = new    Zend_Form_Element_Select("roleid");
		$roleid->setAttrib('id', 'roleid');
		$roleid->addMultiOption("1", "User");
		$roleid->addMultiOption("2", "Admin");
		
		$submit = new    Zend_Form_Element_Submit('btnSaveUser');
		$submit->setLabel('Save');
		$submit->setAttrib('id', 'btnSaveUser');
		
		$this->addElement($name);
		$this->addElement($lastname);
		$this->addElement($email);
		$this->addElement($username);
		$this->addElement($password);
		$this->addElement($roleid);
		$this->addElement($submit);
	}
	
}
