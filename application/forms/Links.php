<?php

class    Application_Form_Links extends Zend_Form {
	
	public function init() {
		$this->setAction('/Admin/index')->setMethod('post');
		
		$name = new    Zend_Form_Element_Text('tbName');
		$name->setAttrib('placeholder', 'Menu link name');
		$name->setAttrib('id', 'tbName');
		$name->setRequired(true);
		$name->addValidator('NotEmpty')->addErrorMessage('Enter menu link name');
		
		$controller = new    Zend_Form_Element_Text('tbUrl1');
		$controller->setAttrib('placeholder', 'Menu link path1');
		$controller->setAttrib('id', 'tbUrl1');
		$controller->setRequired(true);
		$controller->addValidator('NotEmpty')->addErrorMessage('Enter menu link path1');
		
		$action = new    Zend_Form_Element_Text('tbUrl2');
		$action->setAttrib('placeholder', 'Menu link path2');
		$action->setAttrib('id', 'tbUrl2');
		
		$submit = new    Zend_Form_Element_Submit('btnSaveMenuLink');
		$submit->setLabel('Save');
		$submit->setAttrib('id', 'btnSaveMenuLink');
		
		$this->addElement($name);
		$this->addElement($controller);
		$this->addElement($action);
		$this->addElement($submit);
	}
	
}
