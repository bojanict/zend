<?php

class    Application_Form_Register extends Zend_Form {
	
	public function init() {
		$this->setAction('/Login/register')->setMethod('post');
		$this->setAttrib("style", "width:400px;margin:0 auto;border-radius:40px;background: rgba(155, 155, 155, .5);");
		
		$name = new    Zend_Form_Element_Text('tbName');
		$name->setAttrib('placeholder', 'Name');
		$name->setAttrib('id', 'tbName');
		$name->setAttrib("style", "color:black;font-weight:bold;");
		$name->setRequired(true);
		$name->addValidator('NotEmpty')->addErrorMessage('Enter your name');
		
		$lastname = new    Zend_Form_Element_Text('tbLastname');
		$lastname->setAttrib('placeholder', 'Lastname');
		$lastname->setAttrib('id', 'tbLastname');
		$lastname->setAttrib("style", "color:black;font-weight:bold;");
		$lastname->setRequired(true);
		$lastname->addValidator('NotEmpty')->addErrorMessage('Enter your lastname');
		
		$email = new    Zend_Form_Element_Text('tbEmail');
		$email->setAttrib('placeholder', 'Email');
		$email->setAttrib('id', 'tbEmail');
		$email->setAttrib("style", "color:black;font-weight:bold;");
		$email->setRequired(true);
		$email->addValidator('NotEmpty')->addErrorMessage('Enter your email');
		$email->addValidator('EmailAddress')->addErrorMessage('Email format is not good');
		
		$username = new    Zend_Form_Element_Text('tbUsername');
		$username->setAttrib('placeholder', 'Username');
		$username->setAttrib('id', 'tbUsername');
		$username->setAttrib("style", "color:black;font-weight:bold;");
		$username->setRequired(true);
		$username->addValidator('NotEmpty')->addErrorMessage('Enter your username');
		
		$password = new    Zend_Form_Element_Password('tbPassword');
		$password->setAttrib('placeholder', 'Password');
		$password->setAttrib('id', 'tbPassword');
		$password->setAttrib("style", "color:black;font-weight:bold;");
		$password->setRequired(true);
		$password->addValidator('NotEmpty')->addErrorMessage('Enter your password');
		
		$submit = new    Zend_Form_Element_Submit('btnSaveUser');
		$submit->setLabel('Save');
		$submit->setAttrib('id', 'btnSaveUser');
		
		$this->addElement($name);
		$this->addElement($lastname);
		$this->addElement($email);
		$this->addElement($username);
		$this->addElement($password);
		$this->addElement($submit);
	}
	
}
