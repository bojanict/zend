<?php

class    Application_Form_Contact extends Zend_Form {
	
	public function init() {
		$this->setAction('/Index/index')->setMethod('post');
		
		$name = new    Zend_Form_Element_Text('tbName');
		$name->setAttrib('placeholder', 'Name');
		$name->setRequired(true);
		$name->addValidator('NotEmpty')->addErrorMessage('Enter your name');
		$name->setAttrib('id', 'tbName');
		$name->setAttrib('style', 'width:400px;color:white;');
		$name->addFilter('StringToLower');
		
		$email = new    Zend_Form_Element_Text('tbEmail');
		$email->setAttrib('placeholder', 'Email');
		$email->setRequired(true);
		$email->addValidator('EmailAddress')->addErrorMessage('Email format is not good');
		$email->setAttrib('id', 'tbEmail');
		$email->setAttrib('style', 'width:400px;color:white;');
		$email->addFilter('StringToLower');
		$email->addFilter('HTMLEntities');
		$email->addFilter('StringTrim');
		
		$message = new    Zend_Form_Element_Textarea('tbMessage');
		$message->setAttrib('placeholder', 'Message');
		$message->setRequired(true);
		$message->addValidator('NotEmpty')->addErrorMessage('Enter your message');
		$message->setAttrib('id', 'tbMessage');
		$message->setAttrib('style', 'height:200px;min-width:900px;color:white;');
		$message->addFilter('HTMLEntities');
		$message->addFilter('StringToLower');
		$message->addFilter('StringTrim');
		
		$submit = new    Zend_Form_Element_Submit('btnSend');
		$submit->setLabel('Send');
		$submit->setAttrib('id', 'btnSend');
		
		$captcha = new    Zend_Form_Element_Captcha('captcha', array(
			'captcha' => array(
				'captcha' => 'Figlet',
				'wordLen' => 5,
				'timeout' => 300
			)
		));
		$captcha->setLabel('Enter captcha:');
		
		$this->setElementDecorators(array(
			'ViewHelper',
			'Errors',
		));
		
		$this->addElement($name);
		$this->addElement($email);
		$this->addElement($message);
		$this->addElement($captcha);
		$this->addElement($submit);
	}
	
}
