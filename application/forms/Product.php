<?php

class    Application_Form_Product extends Zend_Form {
	
	public function init() {
		$this->setAction('/Admin/index')->setMethod('post');
		$this->setAttrib('enctype', 'multipart/form-data');
		
		$name = new    Zend_Form_Element_Text('tbName');
		$name->setAttrib('placeholder', 'Watch name');
		$name->setAttrib('id', 'tbName');
		$name->setAttrib("style", "margin-top:-30px;");
		$name->setRequired(true);
		$name->addValidator('NotEmpty')->addErrorMessage('Enter watch name');
		
		$brands = new    Zend_Form_Element_Select("ddlBrandId");
		$brands->setAttrib('id', 'ddlBrandId');
		$_brands = DBC::fetchAll("SELECT * FROM brands;");
		foreach ($_brands as $brand) {
			$brands->addMultiOption("{$brand['brandid']}", "{$brand['name']}");
		}
		
		$price = new    Zend_Form_Element_Text('tbPrice');
		$price->setAttrib('placeholder', 'Watch price');
		$price->setAttrib('id', 'tbPrice');
		$price->setRequired(true);
		$price->addValidator('NotEmpty')->addErrorMessage('Enter watch price');
		
		$file = new    Zend_Form_Element_File('file');
		$file->setLabel('Upload images:');
		$file->addValidator('Count', false, array(
			'min' => 1,
			'max' => 3
		));
		$file->addValidator('Size', false, 102400);
		$file->addValidator('Extension', false, 'jpg,png,gif');
		$file->setMultiFile(3);
		$file->setValueDisabled(true);
		
		$description = new    Zend_Form_Element_Textarea('taDescription');
		$description->setAttrib('placeholder', 'Optional description');
		$description->setAttrib('id', 'taDescription');
		$description->setAttrib("style", "height:130px;");
		
		
		$submit = new    Zend_Form_Element_Submit('btnSaveProduct');
		$submit->setLabel('Save');
		$submit->setAttrib('id', 'btnSaveProduct');
		
		$this->addElement($name);
		$this->addElement($brands);
		$this->addElement($price);
		$this->addElement($file);
		$this->addElement($description);
		$this->addElement($submit);
	}
	
}
