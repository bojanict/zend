<?php

class    Application_Form_Comment extends Zend_Form {
	
	public function init() {
		$this->setAction('/Products/index')->setMethod('post');
		$this->setAttrib("style", "width:400px;margin:0 auto;border-radius:40px;background: rgba(155, 155, 155, .5);");
		
		$comment = new    Zend_Form_Element_TextArea('tbComment');
		$comment->setAttrib('placeholder', 'Insert text. . .');
		$comment->setAttrib('id', 'tbComment');
		$comment->setAttrib("style", "color:black;font-weight:bold;height:200px;width:300px;");
		$comment->setRequired(true);
		$comment->addValidator('NotEmpty')->addErrorMessage('Enter your comment');
		
		$submit = new    Zend_Form_Element_Submit('btnSaveComment');
		$submit->setLabel('Save');
		$submit->setAttrib('id', 'btnSaveComment');
		
		$this->addElement($comment);
		$this->addElement($submit);
	}
	
}
