<?php

class Application_Form_Brands extends Zend_Form {
	
	public function init() {
		$this->setAction('/Admin/index')->setMethod('post');
		
		$name = new    Zend_Form_Element_Text('tbName');
		$name->setAttrib('placeholder', 'Brand name');
		$name->setAttrib('id', 'tbName');
		$name->setRequired(true);
		$name->addValidator('NotEmpty')->addErrorMessage('Enter brand name');
		
		$submit = new    Zend_Form_Element_Submit('btnSaveBrand');
		$submit->setLabel('Save');
		$submit->setAttrib('id', 'btnSaveBrand');
		
		$this->addElement($name);
		$this->addElement($submit);
	}
	
	
}

