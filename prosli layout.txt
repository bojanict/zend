<?php print $this->doctype(); ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!--<title>Diminishing by FCT</title>-->
<?php print $this->headTitle(); ?>
<!--<link href="style.css" rel="stylesheet" type="text/css" media="screen" />-->
<?php print $this->headLink(); ?>
<?php print $this->headScript(); ?>
</head>
<body>
<div id="wrapper">
	<div id="menu">
	  <ul>
		<li><a href="<?php print $this->url(array('controller'=>'Index','action'=>'index'),'default',true); ?>">Pocetna</a></li>
                <li><a href="<?php print $this->url(array('controller'=>'Galerija','action'=>'index'),'default',true); ?>">Galerija</a></li>
                <li><a href="<?php print $this->url(array('controller'=>'Formular','action'=>'kontakt'),'default',true); ?>">Kontakt</a></li>
                <li><a href="<?php print $this->url(array('controller'=>'Administracija','action'=>'index'),'default',true); ?>">Administracija</a></li>
               <li><a href="<?php print $this->url(array('controller'=>'Login','action'=>'logout'),'default',true); ?>">Logout</a></li>
               <li><a href="<?php print $this->url(array('controller'=>'Formular','action'=>'login'),'default',true); ?>">Login</a></li>
	   </ul>
	</div>
	<!-- end #menu -->
	<div id="header">
		<div id="logo">
			<h1><a href="<?php print $this->url(array('controller'=>'Index','action'=>'index'),'default',true); ?>">Moj blog</a></h1>
		</div>
		<div id="search">
			<form method="get" action="">
				<fieldset>
					<input type="text" name="s" id="search-text" size="15" value="enter keywords here..." />
					<input type="submit" id="search-submit" value="GO" />
				</fieldset>
			</form>
		</div>
	</div>
	<div id="splash">&nbsp;</div>
	<!-- end #header -->
	<div id="page">
		<div id="page-bgtop">
			<div id="page-bgbtm">
                            <div class="poruka">
                              <?php print $this->layout()->poruka; ?>    
                            </div>
                            
				<div id="content">
                                    <?php echo $this->layout()->content; ?>

					<div style="clear: both;">&nbsp;</div>
				</div>
				<!-- end #content -->
				<div id="sidebar">
                                    <?php print $this->placeholder('sidebar'); ?>
					<!--<ul>
						<li>
							<h2>Aliquam tempus</h2>
							<p>Mauris vitae nisl nec metus placerat perdiet est. Phasellus dapibus semper consectetuer hendrerit.</p>
						</li>
						<li>
							<h2>Categories</h2>
							<ul>
								<li><a href="#">Aliquam libero</a></li>
								<li><a href="#">Consectetuer adipiscing elit</a></li>
								<li><a href="#">Metus aliquam pellentesque</a></li>
								<li><a href="#">Suspendisse iaculis mauris</a></li>
								<li><a href="#">Urnanet non molestie semper</a></li>
								<li><a href="#">Proin gravida orci porttitor</a></li>
							</ul>
						</li>
						<li>
							<h2>Blogroll</h2>
							<ul>
								<li><a href="#">Aliquam libero</a></li>
								<li><a href="#">Consectetuer adipiscing elit</a></li>
								<li><a href="#">Metus aliquam pellentesque</a></li>
								<li><a href="#">Suspendisse iaculis mauris</a></li>
								<li><a href="#">Urnanet non molestie semper</a></li>
								<li><a href="#">Proin gravida orci porttitor</a></li>
							</ul>
						</li>
						<li>
							<h2>Archives</h2>
							<ul>
								<li><a href="#">Aliquam libero</a></li>
								<li><a href="#">Consectetuer adipiscing elit</a></li>
								<li><a href="#">Metus aliquam pellentesque</a></li>
								<li><a href="#">Suspendisse iaculis mauris</a></li>
								<li><a href="#">Urnanet non molestie semper</a></li>
								<li><a href="#">Proin gravida orci porttitor</a></li>
							</ul>
						</li>
					</ul>-->
				</div>
				<!-- end #sidebar -->
				<div style="clear: both;">&nbsp;</div>
			</div>
		</div>
	</div>
	<!-- end #page -->
</div>
<div id="footer-wrapper">
	<div id="three-columns">
		<div id="column1">
			<h2>Consectetuer</h2>
			<ul>
				<li><a href="#">Aliquam libero</a></li>
				<li><a href="#">Consectetuer adipiscing elit</a></li>
				<li><a href="#">Metus aliquam pellentesque</a></li>
				<li><a href="#">Suspendisse iaculis mauris</a></li>
				<li><a href="#">Urnanet non molestie semper</a></li>
				<li><a href="#">Proin gravida orci porttitor</a></li>
			</ul>
		</div>
		<div id="column2">
			<h2>Suspendisse</h2>
			<ul>
				<li><a href="#">Aliquam libero</a></li>
				<li><a href="#">Consectetuer adipiscing elit</a></li>
				<li><a href="#">Metus aliquam pellentesque</a></li>
				<li><a href="#">Suspendisse iaculis mauris</a></li>
				<li><a href="#">Urnanet non molestie semper</a></li>
				<li><a href="#">Proin gravida orci porttitor</a></li>
			</ul>
		</div>
		<div id="column3">
			<h2>Pellentesque</h2>
			<ul>
				<li><a href="#">Aliquam libero</a></li>
				<li><a href="#">Consectetuer adipiscing elit</a></li>
				<li><a href="#">Metus aliquam pellentesque</a></li>
				<li><a href="#">Suspendisse iaculis mauris</a></li>
				<li><a href="#">Urnanet non molestie semper</a></li>
				<li><a href="#">Proin gravida orci porttitor</a></li>
			</ul>
		</div>
	</div>
	<div id="footer">
		<p>Copyright (c) 2011 Sitename.com. All rights reserved. Design by <a href="http://www.freecsstemplates.org/" rel="nofollow">FreeCSSTemplates.org</a>.</p>
	</div>
	<!-- end #footer -->
</div>
</body>
</html>
